# alta3research-mycode-cert

API Design with Python - Alta3 Research Certification Project

## Name
Python scripting - Displaying the endpoints in the form of JSON/HTML with the help of FLASK Library and Demonstration with the requests HTTP library and displaying in the Human readable format

## Description
To perform the API design with the help of python and along with few libraries to get the result directly using the RESTful APIs requests (GET, POST, PUT, DELETE) either in the form of JSON or in the HTML Format and align the obtained output in a neat manner with the help of pprint library in the python. With the help of these obtained data using the API, many process can be carried out like data analysis, data trend, etc..,. In this project, developed a couple of scripts as shown below and their working way is described aside as well.

- First Script - Code Name - Displaying the endpoints in the form of JSON/HTML with the help of FLASK Library - Description - This script has two endpoints - FIRST ENDPOINT will return the pre-defined JSON inside the scripting itself either in the view of webbrowser (windows machine)/in the curl output (linux machine). SECOND ENDPOINT will return the pre-defined index.html (HTML Code) which is linked to the main code as well either in the view of webbrowser (windows machine)/in the curl output (linux machine).
1. For returing FIRST ENDPOINT - JSON - keyin the URL - http://0.0.0.0:2224/returnjson or http://127.0.0.1:2224/returnjson
2. For returing SECOND ENDPOINT - HTML - keyin the URL - http://0.0.0.0:2224 or http://127.0.0.1:2224

- Second Script - Code Name - Demonstration with the requests HTTP library and displaying in the Human readable format - This script will return the webdata of 2 APIs (Mars Rover Photos and Tech Transfer from the NASA Website APIs) and it will convert the data into the JSON format which will then converted to the human readable format with the help of pprint library in the python3. Also, for the Mars Rover Photos API it got a list of page numbers and for that, in the code, a input is been asked from the user on the page number and it will add this page number in the API URL along with the credentials as well to get to the API and fetch the results accordingly.

This script is compatible with both linux and windows machines.

## Prerequisites
Python needs to be installed on the machine where the code is going to be executed and along with that few of the libraries needs to be installed on the same machine as well.

Libraries needed to be installed along with python are -
- requests
- pprint
- flask
- flask_restful

Example - pip install requests

If let's say the python script file is compiled with pyinstaller library as a single executable file means both the python software as well as the above-mentioned libraries are not required to be installed as well.

## Built With
[Python](https://www.python.org/) - The coding language used

## Authors

Annamalai L - (https://annamalai20.wordpress.com/)
