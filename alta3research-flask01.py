#!/usr/bin/python3
## Author - Annamalai Lakshmanan
## Version 1
## Code Name - Displaying the endpoints in the form of JSON/HTML with the help of FLASK Library

## import the flask object and render_template
from flask import Flask, jsonify, render_template
from flask_restful import Api, Resource

## Printing the values and so that user can decide which endpoint can be chosen
print("For returing FIRST ENDPOINT - JSON - keyin the URL - http://0.0.0.0:2224/returnjson or http://127.0.0.1:2224/returnjson")
print("For returing SECOND ENDPOINT - HTML - keyin the URL - http://0.0.0.0:2224 or http://127.0.0.1:2224")

## Define a flask application instance
app =   Flask(__name__)
api =   Api(app)

## Declare the data and shows as in JSON Format
class returnjson(Resource):
    def get(self):
        data=[{
            'Car Name': 'Honda City', 
            'Car Model': 'City', 
            'Car Maker': 'Honda', 
            'Car Price': '20,000 USD'
            }, 
            {
                'Car Name': 'Bugatti Chiron', 
                'Car Model': 'Chiron', 
                'Car Maker': 'Bugatti', 
                'Car Price': '3 Million USD'
                }
            ]
        print("=========== FIRST ENDPOINT ===========")
        print("============= JSON DATA ==============")
        return jsonify(data)
        print("======================================")

api.add_resource(returnjson,'/returnjson')

## Declare the condition and shows the data in HTML Format
## specify a app route ('/') to handle incoming web requests
@app.route('/')
def home():
    # return your html file
    print("=========== SECOND ENDPOINT ===========")
    print("============== HTML DATA ==============")
    return render_template('index.html')
    print("=======================================")
  
## Maincode to initiate the program to show both HTML and JSON Endpoints  
if __name__=='__main__':
    app.run(host="0.0.0.0", port=2224)
